/**
 * --------------------------- IMPORT nessaicaire ------------------------------------
 */
require('isomorphic-fetch');
const path = require('path') // gestion fichiers locaux
let bodyParser = require('body-parser');
const config = require(path.join(__dirname, 'config.js')) // config dans fichier config.js
const mongoose = require('mongoose');//mongoose
const { Schema } = mongoose;//shcema de mongoose


/**
 * --------------------------- MONGOOSE ------------------------------------
 */
async function effacerbd() {
    const conn = mongoose.createConnection('mongodb://' + config.mongodb.host + '/' + config.mongodb.db);
    // Deletes the entire 'mydb' database
    console.log("---------------------------------------------------------------------------------------")
    console.log("------------------------Suppression des données de la BD-------------------------------")
    console.log("---------------------------------------------------------------------------------------")
    await conn.dropDatabase().then(
        console.log("suppression terminée")
    );
}

effacerbd();
// ----------------connexion  base de donnee mongodb----------------------
mongoose.connect('mongodb://' + config.mongodb.host + '/' + config.mongodb.db)
mongoose.connection.on('error', err => {
    console.error(err)
})




//recuperer les models mis dans un fichier separé
const models = require('./models')(mongoose);


var id_artistes = [
    "111881",//the Weeknd 
    "112024",//Nas
    "111318",//tupac,
    "111304",//Eminem,
    "112424",//Michael Jackson,
    "111492",//Daft Punk
    ]


// fetch info artiste
async function recupererInfoArtiste(id) {

    var artiste;
    await fetch("https://theaudiodb.com/api/v1/json/2/artist.php?i=" + id)
        .then((response) => { return response.json() })
        .then(
            async function (data) {
                let infoArtiste = data.artists[0]


                artiste = new models.Artiste({
                    nom: infoArtiste.strArtist,
                    anneeNaissance: infoArtiste.intBornYear,
                    ville: infoArtiste.strCountry,
                    label: infoArtiste.strLabel,
                    style: infoArtiste.strStyle,
                    bio: infoArtiste.strBiographyFR,
                    url_image: infoArtiste.strArtistThumb,
                    url_banniere: infoArtiste.strArtistBanner,
                    albums: await recupererAlbumArtiste(infoArtiste.idArtist).then(
                        data => { return data })
                });
                    //sauvegarder les donnees en bd :
                    //artiste._id = infoArtiste.idArtist

                    artiste.save().then( async function  (artiste) {
                    console.log("artiste:" ,artiste.nom ,artiste._id )
                    
                    }
                 
                    )
      

            }
        )








}






async function recupererAlbumArtiste(id_artiste) {
    var les_albums = []


    await fetch("https://theaudiodb.com/api/v1/json/2/album.php?i=" + id_artiste)
        .then((response) => { return response.json() })
        .then(
            (data) => {
                return data

            }
        )

        .then(
            function (data) {

                    data["album"].forEach(async function (album) {
                    //creer un objet mongoose
                    let unalbum = new models.Album({
                        
                        nom: album.strAlbum,
                        annee: album.intYearReleased,
                        genre: album.strStyle,
                        url_image: album.strAlbumThumb,
                        titres: await recupererTitreAlbum(album.idAlbum).then(data => { return data })
                    })
                    //unalbum.artiste = id_artiste
                    //sauvegarder bd
                    unalbum.save().then(
                        les_albums.push(unalbum._id)
                    )

                    })
                


            })
    //bug retourne les_album = [] donc ajout fonction delay pour attendre que le fetch remplisse les_album
    await delay(2);
    return les_albums
}

//les titre



async function recupererTitreAlbum(id_album) {

    let les_tracks = []
    await fetch("https://theaudiodb.com/api/v1/json/2/track.php?m=" + id_album)

        .then((response) => { return response.json() })
        .then(
            (data) => {
                return data

            }
        )

        .then(
            (data) => {


                data["track"].forEach(track => {

                    //creer un objet mongoose
                    let titre = new models.Titre({
                        nom: track.strTrack,
                        duree: track.intDuration,
                        genre: track.strGenre
                    });

                    //sauvegarder dans bd
                    titre.save().then(
                        //console.log(track.strTrack,"titre ajoute ")
                        les_tracks.push(titre._id)
                    )

                    
                });


            }
        )
    return les_tracks;
}



// fonction delay attend n sec pour corrigier probleme tableau retourné vide dans recupererAlbumArtiste
function delay(n) {
    return new Promise(function (resolve) {
        setTimeout(resolve, n * 1000);
    });
}

async function recupererDonnees() {

    console.log("---------------------------------------------------------------------------------------")
    console.log("-----------------------------ajout de données dans la BD-------------------------------")
    console.log("---------------------------------------------------------------------------------------")

    id_artistes.forEach(async function (id) {
        await recupererInfoArtiste(id)

    });

}

recupererDonnees()


setTimeout((function() {
    return process.exit(22);
}), 20000);

process.on('exit', function() {
    return console.log(`Sortie du script du generation de données`);
});