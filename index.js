/**
 * --------------------------- IMPORT nessaicaire ------------------------------------
 */

const path = require('path') // gestion fichiers locaux
const express = require('express') //framework mvc
const nunjucks = require('nunjucks') // templates
let bodyParser = require('body-parser');
const config = require(path.join(__dirname, 'config.js')) // config dans fichier config.js
const mongoose = require('mongoose');//mongoose
const { Schema } = mongoose;//shcema de mongoose

//authentification
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var logger = require('morgan');
var cookieParser = require('cookie-parser');


/**
 * --------------------------- MONGOOSE ------------------------------------
 */


// ----------------connexion  base de donnee mongodb----------------------
mongoose.connect('mongodb://' + config.mongodb.host + '/' + config.mongodb.db)
mongoose.connection.on('error', err => {
  console.error(err)
})


//recuperer les models mis dans un fichier separé
const models = require('./models')(mongoose);



/**
 * --------------------------- EXPRESS ------------------------------------
 */





let app = express()

//nunjuck
nunjucks.configure('views', {
  express: app
})



//utilise le dossier views comme static
app.use(express.static(path.join(__dirname, '/views')))







//authentification service

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(models.User.authenticate()));
passport.serializeUser(models.User.serializeUser());
passport.deserializeUser(models.User.deserializeUser());

//routeur
let router = express.Router()
//utilise le router
app.use(router);

/**
 * --------------------------- ROUTAGE et REQUETE ------------------------------------
 */


//---------------------authentification ----------------------------------

//inscription
router.route('/register')
  .get((req, res) => {
    res.render('register.njk')
  })

  .post(function(req, res) {
    models.User.register(new models.User({ username : req.body.username , albums: [] }), req.body.password, function(err, account) {
        if (err) {
            return res.render('register', { user : user });
        }

        passport.authenticate('local')(req, res, function () {
            res.redirect('/');
        });
    });
});

// connexion
router.route("/login")
.get(function(req, res) {
  res.render('login.njk', { user : req.user });
})
.post( passport.authenticate('local'), function(req, res) {
  res.redirect('/');
});

//deconnexion
router.route('/logout')
.get( function(req, res) {
  req.logout();
  res.redirect('/');
});


//---------------------- les selections user -----------------------------------

// voir la liste des selection d' un user 
router.route('/liste')
.get((req,res) => {
  console.log(req.user._id)
  let user = models.User.findById( req.user._id ).then((user) => {
    user.populate('albums')
    .then( (user) => {
    res.render('liste.njk' ,{user: user})
    }

    )
  })


})

//------------------ajouter album aux selection d'un user
router.route('/liste/ajouter')
.post((req,res) => {

  let user = models.User.findById( req.user._id ).then((user) => {
    les_albums = user.albums
    les_albums.push(req.body.album_id)
    user.albums = les_albums
    user.save()
    res.redirect('/liste')
  }
  )

})

//supprimer de ma liste 
router.route('/liste/supprimer')
.post((req,res) => {

  let user = models.User.findById( req.user._id ).then((user) => {
    les_albums = user.albums
    // supprimer l'element 
    const index = les_albums.indexOf(req.body.album_id);
    if (index > -1) {
      les_albums.splice(index, 1); // 2nd parameter means remove one item only
    }
    user.albums = les_albums
    user.save()
    res.redirect('/liste')
  }
  )

})




//-----------------------acceuil => affiche les derniers albums 
router.route('/')
  .get((req, res) => {
    models.Album.find().then(albums => {
        res.render('accueil.njk', { albums: albums ,user: req.user })
      }).catch(err => {
        console.error(err)
      })
  })





//--------------- ARTISTES ---------------------
//artistes => affiche la listes des artiste
router.route('/artistes')
  .get((req, res) => {
    models.Artiste.find().then(artistes => {
        res.render('artiste/liste-artistes.njk', { artistes: artistes,user: req.user  })
      }).catch(err => {
        console.error(err)
      })
  })

// info d'un artiste  + ses albums
router.route('/artistes/voir/:id')
  .get((req, res) => {
    models.Artiste.findById(req.params.id)
    .populate("albums")
    .then(artiste => {
        res.render('artiste/artiste.njk', { artiste: artiste,user: req.user  })
      }).catch(err => {
        console.error(err)
      })
  })

  //modifier artiste : afficher formulaire de modification artiste
  router.route('/artistes/edit/:id')
  .get((req, res) => {
    models.Artiste.findById(req.params.id)
    .populate("albums")
    .then(artiste => {
        res.render('artiste/edit-artiste.njk', { artiste: artiste,user: req.user  })
      }).catch(err => {
        console.error(err)
      })
  })
  .post((req, res) => {
    models.Artiste.findById(req.params.id).then(artiste => {
    
      //recuperer les info modfifié et enregister
      artiste.nom = req.body.nom
      artiste.anneeNaissance = req.body.anneeNaissance
      artiste.ville = req.body.ville
      artiste.label = req.body.label
      artiste.style = req.body.style
      artiste.bio = req.body.bio
      artiste.url_image = req.body.url_image
      artiste.url_banniere = req.body.url_banniere

      //pas de modification de la liste des albums pr l'instant


      artiste.save().then(artiste => {
        console.log(`artiste: ${artiste.nom}  modifié`);
        //rediriger vers page de l'artiste
        res.redirect('/artistes/voir/'+req.params.id)
      }).catch(err => {
        console.error(err)
      })


    }).catch(err => {
      console.error(err)
    })
  });


  //----------------ajouter un artiste avec albums vides ----------------------
  router.route('/artistes/add')
  .get((req, res) => {
        res.render('artiste/add-artiste.njk',{user: req.user} )
  })
  .post((req, res) => {
    //creer un instance artiste a partir du formulaire recuperer
    let newArtiste = new models.Artiste({
      _id: new mongoose.Types.ObjectId(), //id généré
      nom : req.body.nom,
      anneeNaissance : req.body.anneeNaissance,
      ville : req.body.ville,
      label : req.body.label,
      style : req.body.style,
      bio : req.body.bio,
      url_image : req.body.url_image,
      url_banniere : req.body.url_banniere,
    }
    )

    //ajouter en bd
    newArtiste.save().then(artiste => {
      console.log(`artiste: ${artiste.nom}  ajouté`);
      //rediriger vers page de l'artiste ajouté
      res.redirect('/artistes/voir/'+artiste._id)
    }).catch(err => {
      console.error(err)
    })

  })

  //---------------- supprimer un artiste ---------------
  //( ajouter un bouton de suppression dans la page affichant l'artiste 
  // bouton dans un formulaire post allant vers =>)

  router.route('/artistes/delete/:id')
  .post((req,res) => {
    models.Artiste.findByIdAndRemove(req.params.id).then(() => {
      console.log('artiste supprimé')
      //rediriger vers page des artistes
      res.redirect('/artistes/')
    }).catch(err => {
      console.error(err)
    })
  })



//------------ ALBUM ----------------
//albums => affiche les albums
router.route('/albums')
  .get((req, res) => {
    models.Album.find()
    .populate('titres') // recupere  les titre avec les id enregistré dans le modele 
    .then(albums => {
        res.render('album/liste-albums.njk', { albums: albums,user: req.user  })
      }).catch(err => {
        console.error(err)
      })
  })

  //affiche les info d'un album : info plus les titres d'un albums
  router.route('/albums/voir/:id')
  .get((req, res) => {
    console.log(req.user)
    models.Album.findById(req.params.id)
    .populate('titres') 
    .then(album => {
        res.render('album/album.njk', { album: album,user: req.user  })
      }).catch(err => {
        console.error(err)
      })
  })

  //modifier un album : afficher formlulaire de modification :
  router.route('/albums/edit/:id')
  .get((req, res) => {
    var artistes = models.Artiste.find() // tout les artiste à mettre dans select
    models.Album.findById(req.params.id)
    .populate("titres")
    .then(album => {
        res.render('album/add-album.njk', { album: album ,artistes:artistes,user: req.user })
      }).catch(err => {
        console.error(err)
      })
  })
  .post((req, res) => {
    models.Album.findById(req.params.id).then(album => {
    
      //recuperer les info modfifié et enregister
      album.nom = req.body.nom
      album.annee = req.body.annee
      album.genre = req.body.genre
      album.label = req.body.label
      album.url_image = req.body.url_image
      album.artiste = req.body.artiste_id

      //pas de modification de la liste des albums pr l'instant


      album.save().then(album => {
        console.log(`album: ${album.nom}  modifié`);
        //rediriger vers page de l'artiste
        res.redirect('/albums/voir/'+req.params.id)
      }).catch(err => {
        console.error(err)
      })


    }).catch(err => {
      console.error(err)
    })
  });


   //----------------ajouter un album avec titres vides ----------------------
   router.route('/albums/add')
   .get((req, res) => {
    models.Artiste.find().then( artistes => 
      res.render('album/add-album.njk',{artistes: artistes,user: req.user })
    ) // tout les artiste à mettre dans select

   })
   .post((req, res) => {
     //creer un instance album a partir du formulaire recuperer
     let newAlbum = new models.Album({
       _id: new mongoose.Types.ObjectId(), //id généré
       nom : req.body.nom,
       annee : req.body.annee,
       genre : req.body.genre,
       url_image : req.body.url_image,
       artiste: req.body.artiste_id,
       titres:[]
     }
     )
 
     //ajouter en bd
     newAlbum.save().then(album => {
       console.log(`album: ${album.nom}  ajouté`);
       //rediriger vers page de l'artiste ajouté
       res.redirect('/albums/voir/'+album._id)
     }).catch(err => {
       console.error(err)
     })
 
   })


     //---------------- supprimer un artiste ---------------
  //( ajouter un bouton de suppression dans la page affichant l'artiste 
  // bouton dans un formulaire post allant vers =>)

  router.route('/albums/delete/:id')
  .post((req,res) => {
    models.Album.findByIdAndRemove(req.params.id).then(() => {
      console.log('album supprimé')
      //rediriger vers page des artistes
      res.redirect('/')
    }).catch(err => {
      console.error(err)
    })
  })


/**
 * --------------------------- LANCER SERVER ------------------------------------
 */



//lance le serveur
app.listen(config.express.port, config.express.ip, () => {
  console.log('Server listening on http://' + config.express.ip + ':' + config.express.port)
})
