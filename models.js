

const mongoose = require('mongoose');//mongoose
const { Schema } = mongoose;//shcema de mongoose
var passportLocalMongoose = require('passport-local-mongoose');

module.exports= function (mongoose) {
    // les schemas :
const ArtisteSchema = new mongoose.Schema(
    {   
        nom: { type: String, required: true },
        anneeNaissance: { type: String, required: true },
        ville: String,
        label: String,
        style: String,
        bio: String,
        url_image: String,
        url_banniere: String,
        albums: [ { type: Schema.Types.ObjectId, ref: 'Album' }]
    }
);

const AlbumSchema = new mongoose.Schema(
    {
        nom:{ type: String, required: true },
        annee: String,
        genre: String,
        url_image: String,
        artiste : { type: Schema.Types.ObjectId, ref: 'Artiste' },
        titres: [ { type: Schema.Types.ObjectId, ref: 'Titre' }]
    }
);

const TitreSchema = new mongoose.Schema(
    {
        nom:{ type: String, required: true },
        duree: String,
        genre: String,
        album : { type: Schema.Types.ObjectId, ref: 'Album' },
    }
);


const UserSchema = new mongoose.Schema({
    username: String,
    password: String,
    albums: [ { type: Schema.Types.ObjectId, ref: 'Album' }]

});

UserSchema.plugin(passportLocalMongoose);

// les models :

const models ={
Artiste : mongoose.model('Artiste', ArtisteSchema),
 Album : mongoose.model('Album', AlbumSchema),
 Titre : mongoose.model('Titre', TitreSchema),
 User : mongoose.model('User', UserSchema),
}

return models

}

