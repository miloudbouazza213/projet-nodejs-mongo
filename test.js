/**
 * --------------------------- IMPORT nessaicaire ------------------------------------
 */
 require('isomorphic-fetch');
 const path = require('path') // gestion fichiers locaux
 let bodyParser = require('body-parser');
 const config = require(path.join(__dirname, 'config.js')) // config dans fichier config.js
 const mongoose = require('mongoose');//mongoose
 const { Schema } = mongoose;//shcema de mongoose
 
 
 /**
  * --------------------------- MONGOOSE ------------------------------------
  */
 async function effacerbd() {
     const conn = mongoose.createConnection('mongodb://' + config.mongodb.host + '/' + config.mongodb.db);
     // Deletes the entire 'mydb' database
     console.log("---------------------------------------------------------------------------------------")
     console.log("------------------------Suppression des données de la BD-------------------------------")
     console.log("---------------------------------------------------------------------------------------")
     await conn.dropDatabase().then(
         console.log("suppression terminée")
     );
 }
 
 effacerbd();
 // ----------------connexion  base de donnee mongodb----------------------
 mongoose.connect('mongodb://' + config.mongodb.host + '/' + config.mongodb.db)
 mongoose.connection.on('error', err => {
     console.error(err)
 })
 
 
 
 
 //recuperer les models mis dans un fichier separé
 const models = require('./models')(mongoose);
 

let artiste = new models.Artiste({
    nom: "test",
    anneeNaissance: "infoArtiste.intBornYear",
    ville: "infoArtiste.strCountry",
    label: "infoArtiste.strLabel",
    style: "infoArtiste.strStyle",
    bio: "infoArtiste.strBiographyFR",
    url_image: "infoArtiste.strArtistThumb",
    url_banniere: "infoArtiste.strArtistBanner",
    albums: []
});


artiste.save().then( () => {
    console.log("artiste ")
    console.log(artiste._id)
    let unalbum = new models.Album({
        nom: "album.strAlbum",
        annee: "album.intYearReleased",
        genre: "album.strStyle",
        url_image: "album.strAlbumThumb",
        artiste: artiste._id
    
    })
    
    
    unalbum.save().then(() => {
            console.log(" album ")
    console.log(unalbum._id) 
    let titre = new models.Titre({
        album:unalbum._id,
        nom: "track.strTrack",
        duree: "track.intDuration",
        genre: "track.strGenre"
    });
    titre.save().then(
        console.log("titre ajoute ")
    )
    
    models.Album.find({artiste:artiste._id})
.populate('artiste')
 models.Album.find({artiste:artiste._id})
 .then((album) => console.log("test album",album))
  })
}
    
)






